#include <stdio.h>
#include "../src/context.h"
#include "../src/lex.h"

static void usage() {
  char* usage = "usage: lexdump <filename>\n\n"
    "After parsing a quincer input file, dump the parse\n"
    "results to output for test compliance\n\n"
    "The output might be valid YAML.";
  fprintf(stderr,usage);
}

int main(int argc, char** argv) {
  if(argc < 2) {
    usage();
    return 1;
  }

  printf("input:\n");

  qn_config_t* config = parse_config(argv[1]);

  int i;
  for(i=0; i<config->noutports; i++) {
    if(!i) {
      printf("  ports:\n");
    }
    printf("  - %s\n",config->outports[i]);
  }

  for(i=0; i<config->nvoices; i++) {
    if(!i) {
      printf("  voices:\n");
    }
    qn_voice_t* v = config->voices+i;
    printf("  - name: %s\n",v->name);
    printf("    portname: %s\n", v->portname);
    printf("    channel: %d\n", v->channel+1);
  }

  for(i=0; i<config->npatterns; i++) {
    if(!i) {
      printf("  patterns:\n");
    }
    qn_pattern_t* p = config->patterns+i;
    printf("  - bpm: %d\n",p->beats_per_minute);
    printf("    bpm_relationship: %s\n",
	   (p->bpm_relation==Absolute) ? "absolute" : "relative");
    printf("    swing: %f\n", p->swing_value);
    printf("    ticks_per_beat: %d\n", p->tbm_ticks_per_beat);
    printf("    beats_per_bar: %f\n", p->tbm_beats_per_bar);
    printf("    patternvoices:\n");
    int j;
    for(j=0;j<p->npatternvoices;j++) {
      qn_patternvoice_t* pv = p->patternvoices + j;
      printf("    - voice: %s\n",pv->voice->name);
      printf("      nbeats: %d\n",pv->nbeats);
      printf("      nevents: %d\n",pv->nevents);
      printf("      events:\n");
      int k;
      for(k=0;k<pv->nevents;k++) {
	qn_event_t* evt = pv->events+k;
	printf("      - start: %d\n",evt->start_tick);
	printf("        data:\n");
	int l;
	for(l=0;l<NBYTES_MIDI;l++) {
	  unsigned char c = evt->data[l];
	  printf("        - byte %d: %d\n",l,c);
	} // loop data bytes
      } // loop events
    } // loop patternvoices
  } // loop patterns

  return 0;
}
