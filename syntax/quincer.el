;; https://www.emacswiki.org/emacs/GenericMode
(require 'generic-x)
(define-generic-mode
    'quincer-mode                  ;; name of the mode to create
  nil
  '("run" "once" "loop"
    "voice"
    "pattern"
    "cc" "pc" "note"
    "include"
    "clock")                        ;; some keywords
  '(
    ;; comments.  Require # preceded by space or at line-start
    ;; otherwise the note 'f#' looks like a comment start
    ("\\(\\(^\\)\\|\\( \\)\\)\\(#.*\\)$" (4 'font-lock-comment-face))
    
    ;; highlight output name in clock declaration
    ("^clock[ ]*\\(\\sw*\\)[ ]*.*" (1 'font-lock-string-face))

    ;; highlignt output and voice names in voice declaration
    ("^voice[ ]*\\(\\sw*\\)[ ]*\\(\\sw*\\)[ ]*.*"
     (1 'font-lock-variable-name-face) (2 'font-lock-string-face))

    ;; highlight event definition line
    ("^\\(.\\) [^|\n]*$" (1 'font-lock-regexp-grouping-construct))

    ;; highlight tick line
    ("^[0.'S|]\\{4,\\}$" . 'font-lock-function-name-face)

    ;; highlight notes, pipe and voice name in pattern line
    ("^\\([^|]*\\)\\(|\\)[ ]*\\(\\sw*\\)$" (1 'font-lock-regexp-grouping-construct)
     (2 'font-lock-function-name-face) (3 'font-lock-variable-name-face))
    )
  '("\\.qn$")                      ;; files for which to activate this mode
  nil                              ;; other functions to call
  "A mode for quincer files"       ;; doc string for this mode
  )
