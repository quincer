" Vim syntax file
"
" Language: Quincer
" Author: Tassilo Philipp
" Version: 0.2

syntax clear

syntax match qnNumber "[0-9.]\+"

syntax match qnNoteDefinition "^[^0-9]\s\+[0-9a-gA-G#-]\+\s\+[0-9]\+\s*$" contains=qnNoteDefChar,qnNamedPitch,qnNumber
syntax match qnNoteDefChar "^[^0-9]" contained contains=@qnNotes,qnNote
syntax match qnNamedPitch "\s\zs[a-gA-G][b#]\?\(-1\|[0-9]\)*" contained

syntax match qnEventLine "\p\+|\s*[a-zA-Z0-9]\+" contains=qnEventLineEnd,qnEvent,qnEventNotes transparent
syntax match qnEventLineEnd "|\s*[a-zA-Z0-9]\+" contains=qnEventLoop,qnVoiceName transparent
syntax match qnEventLoop "|" contained
syntax match qnEventNotes "^[^|#]\+" contained contains=@qnNotes,qnNote

syntax match qnVoiceDecl "^\s*voice\s\+[a-zA-Z0-9]\+" contains=qnVoiceName,qnKeyword transparent
syntax match qnVoiceName "[a-zA-Z0-9]\+" contained

syntax keyword qnKeyword voice contained
syntax keyword qnKeyword pattern

" does not cover all printable range
if exists('g:quincer_seperate_note_colors') && g:quincer_seperate_note_colors
	let a = ['ctermfg', 'ctermbg']
	let ai = exists('g:quincer_seperate_note_colors_inv') && g:quincer_seperate_note_colors_inv
	let x = 33     " first non-blank, useful ascii: !
	while x <= 126 " last ascii: ~
		execute "syntax cluster qnNotes add=qnNote_".x
		execute 'syntax match qnNote_'.x.' "\%d'.x.'" contained'
		execute "hi QuincerNote_".x." ".a[ai]."=".x
		execute "hi link qnNote_".x." QuincerNote_".x
		let x += 1
	endwhile
else
    syntax match qnNote "\p" contained
endif

" Note prolongation covered separately
syntax cluster qnNotes add=qnEventProlong
syntax match   qnEventProlong "-" contained

" Keep this after event line definitions, as they override them
syntax match   qnBar "[.'|]*0[.]\+'[.'S|]\+" contains=qnBarMarker,qnBarTick,qnBarBeat transparent
syntax match   qnBarMarker "[0S|]" contained
syntax match   qnBarTick "\." contained
syntax match   qnBarBeat "'" contained

" Comments override all, above, so keep here
syntax match   qnComment "#.*$"

" Links to default types
hi link qnComment      Comment
hi link qnNumber       Number
hi link qnNamedPitch   Special
hi link qnKeyword      Type

hi link qnBarMarker    Special
hi link qnBarTick      Comment
hi link qnBarBeat      Label
hi link qnVoiceName    Label

hi link qnEventProlong Label
hi link qnEventLoop    Special

hi link qnNote         Identifier
 
" Visual helpers
hi ColorColumn  cterm=none ctermbg=235 guibg=#222222
hi CursorColumn cterm=none ctermbg=235 guibg=#222222
hi CursorLine   cterm=none ctermbg=235 guibg=#222222

"setlocal cursorcolumn
"setlocal cursorline

let b:current_syntax = "quincer"

