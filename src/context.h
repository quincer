#ifndef __context_h__
#define __context_h__

#include <jack/jack.h>

typedef struct {
  jack_port_t* jackport;
  int clock_numerator;
  int clock_denominator;
  jack_nframes_t clock_next_tick;
  int clock_tick_index;
} qn_port_t;

typedef struct {
  char* name;
  int clock_numerator;
  int clock_denominator;
} qn_config_port_t;

typedef struct {
  char* name;
  char* portname;
  int channel;

  qn_port_t* port; // NULL at parse time, set at init time
} qn_voice_t;

#define NBYTES_MIDI 3
// Used to sort all the output events for a port
typedef struct {
  jack_nframes_t at;
  int szdata;
  char data[NBYTES_MIDI];
} qn_outevent_t;

typedef struct {

  int start_tick;

  // TODO:
  /* int offset_from_tick; */
  /* foo_t offset_type; */

  char data[NBYTES_MIDI];

} qn_event_t;


enum eventset {
  // When handling the prefix events of the next pattern
  PREFIX,

  // When handling the main events of the current pattern
  CURRENT,

  // When handling the suffix events of the previous pattern
  SUFFIX,

  NEVENTSETS
};

typedef struct {
  qn_voice_t* voice;
  int nbeats;
  
  int nevents;
  qn_event_t* events;

  int playhead[NEVENTSETS];
  int playhead_tmp[NEVENTSETS];
} qn_patternvoice_t;

enum BpmRelation {
  Absolute,
  RelativeToBase
};

typedef struct {
  int npatternvoices;
  qn_patternvoice_t* patternvoices;

  float swing_value;
  int tbm_ticks_per_beat;
  float tbm_beats_per_bar;
  float tbm_beat_type;
  int beats_per_minute;
  enum BpmRelation bpm_relation;
  qn_patternvoice_t* longest_pv;
} qn_pattern_t;

typedef qn_pattern_t* (*next_pattern_proto)(void* ctx);
qn_pattern_t* loop_one(void* ctx);
qn_pattern_t* once_through(void* ctx);
qn_pattern_t* loop_all(void* ctx);

typedef struct {
  int noutports;
  qn_config_port_t* outports;
  
  int nvoices;
  qn_voice_t* voices;

  int npatterns;
  qn_pattern_t* patterns;

  // Optional, can override the default
  next_pattern_proto next_pattern_func;
} qn_config_t;

typedef struct {
  jack_client_t* client;
  qn_port_t** ports;
  int base_beats_per_minute; // current, user can change real-time

  qn_pattern_t* prev;
  qn_pattern_t* curr;
  
  jack_nframes_t sample_rate;
  jack_nframes_t elapsed;
  jack_transport_state_t transtate;

  // We are the jack timebase master
  int tbm_bar;
  int tbm_beat;
  int tbm_tick;
  double tbm_bar_start_tick;
  jack_nframes_t frame_in_tick;

  int awaiting_common_start_beat;
} qn_runstate_t;

typedef struct {
  qn_runstate_t* run;
  qn_config_t* config;

  // A pluggable function determines the next pattern.
  // This paves the way for a live mode vs song mode, or
  // various test patterns.
  next_pattern_proto next_pattern_func;
} qn_context_t;

qn_config_t* qn_get_fake_config();
void qn_init_config(qn_config_t* config);
qn_context_t* qn_init_context(qn_config_t* config, jack_client_t* client);
void qn_reset_playhead(qn_context_t* ctx);
void qn_free_config(qn_config_t* cfg);
void qn_free_context(qn_context_t* ctx);
int cmp_outevent(const void* a, const void* b);

  
#endif /* __context_h__ */
