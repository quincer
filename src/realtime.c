#include <jack/jack.h>
#include <jack/midiport.h>
#include "context.h"
#include "realtime.h"
#include <string.h>
#include <stdio.h>

static int get_effective_bpm(qn_pattern_t* pat, int base_beats_per_minute) {
  int bpm = base_beats_per_minute;
  if(pat->bpm_relation == RelativeToBase && bpm+pat->beats_per_minute > 0) {
    bpm += pat->beats_per_minute;
  }
  return bpm;
}

static void set_playheads(qn_context_t* ctx, int pattern_advanced, enum eventset evtset) {

  qn_pattern_t *start_pattern = NULL, *newest_pattern = NULL;
  switch(evtset) {
  case PREFIX:
    start_pattern = newest_pattern = ctx->next_pattern_func(ctx);
    break;
  case CURRENT:
    start_pattern = newest_pattern = ctx->run->curr;
    if(pattern_advanced) {
      newest_pattern = ctx->next_pattern_func(ctx);
    }
    break;
  case SUFFIX:
    start_pattern = newest_pattern = ctx->run->prev;
    if(pattern_advanced) {
      start_pattern = NULL;
    }
    break;
  case NEVENTSETS:
    break;
  }

  int k;
  if(pattern_advanced && start_pattern) {
    // Reset playhead to 0 for the past pattern.  If the
    // newest pattern is the same as the old one, the next
    // block will overwrite the 0 with playhead_tmp
    for(k=0;k<start_pattern->npatternvoices;k++) {
      qn_patternvoice_t* pv = start_pattern->patternvoices + k;
    set_playheads_zero:
      pv->playhead[evtset] = 0;
    }
  }

  // Set playhead for the newest pattern.
  if(newest_pattern) {
    for(k=0;k<newest_pattern->npatternvoices;k++) {
      qn_patternvoice_t* pv = newest_pattern->patternvoices + k;
    set_playheads_value:
      pv->playhead[evtset] = pv->playhead_tmp[evtset];
    }
  }

}

static void accumulate_clock_events(qn_context_t* ctx, qn_port_t* port,
				    int* nout, qn_outevent_t* out,
				    jack_nframes_t fill_last_nframes,
				    int* start_message_sent) {

  if(!port->clock_numerator) return;

  jack_nframes_t next_tick = port->clock_next_tick;
  if(next_tick >= fill_last_nframes) {
    port->clock_next_tick = next_tick - fill_last_nframes;
    return;
  }

  // We have a clock event to emit
  qn_outevent_t* outev = out + *nout;
  outev->at = next_tick;
  outev->data[0] = 0xf8; // clock message
  outev->szdata = 1;
  *nout = *nout+1;
  
  qn_runstate_t* run = ctx->run;
  jack_transport_state_t transport
    = jack_transport_query(run->client, NULL);

  // Re-calculate clock_next_tick
  int curr_bpm = get_effective_bpm(run->curr,run->base_beats_per_minute);
  int curr_beat_len = (run->sample_rate * 60) / curr_bpm;
  int is_beat_start = (port->clock_tick_index == 0);

  // Compensate for rounding errors
  jack_nframes_t tick_interval =
    (curr_beat_len * port->clock_numerator) /
    (24 * port->clock_denominator);

  port->clock_tick_index++;
  if(port->clock_tick_index == 24) {
    port->clock_tick_index = 0;
    jack_nframes_t round_error = (curr_beat_len * port->clock_numerator) - (tick_interval * 24 * port->clock_denominator);
    next_tick += round_error;
  }

  if(transport == JackTransportStopped) {
    run->awaiting_common_start_beat = 1;
  } else if(transport == JackTransportRolling &&
	    is_beat_start &&
	    run->awaiting_common_start_beat) {
    run->awaiting_common_start_beat = 0;

    // send start message
    outev = out + *nout;
    outev->at = 0; //next_tick;
    outev->data[0] = 0xfa; // start message
    outev->szdata = 1;
    *nout = *nout+1;

    // inform caller of the event
    *start_message_sent = 1;

    // set all playheads to -next_tick
    int i;
    for(i=0; i<run->curr->npatternvoices; i++) {
      qn_patternvoice_t* pv = run->curr->patternvoices + i;
      pv->playhead_tmp[CURRENT] = pv->playhead[CURRENT] = -next_tick;
    }
  }

 clock_ticked:
  port->clock_next_tick = tick_interval - (fill_last_nframes - next_tick);
}

static void accumulate_pattern_events(qn_context_t* ctx, qn_port_t* port,
				      qn_pattern_t* pat, int* nout, qn_outevent_t* out,
				      jack_nframes_t fill_last_nframes,
				      int* pattern_advanced, enum eventset evtset) {

  jack_nframes_t fill_last_nframes_orig = fill_last_nframes;
  qn_runstate_t* run = ctx->run;

  // If we straddle two patterns, the latter one will only
  // want to fill a subset of frames with events.
  while(fill_last_nframes) {
    // Inside this loop assumes we're working within a single pattern
    // (pat).  If we advance to a new pattern, we advance this loop and
    // reduce fill_last_nframes.

    int pattern_advanced_now = 0;

    if(!pat) return;

    int k;
    for(k=0;k<pat->npatternvoices;k++) {
      qn_patternvoice_t* pv = pat->patternvoices + k;
      pv->playhead_tmp[evtset] = pv->playhead[evtset];
    }

    int curr_bpm = get_effective_bpm(run->curr,run->base_beats_per_minute);
    int this_bpm = get_effective_bpm(pat,run->base_beats_per_minute);
    int curr_beat_len = (run->sample_rate * 60) / curr_bpm;
    int this_beat_len = (run->sample_rate * 60) / this_bpm;

    // When the longest pv completes, the pattern advances from curr
    // to next.
    jack_nframes_t til_curr_pattern_end =
      (run->curr->longest_pv->nbeats * curr_beat_len)
      - run->curr->longest_pv->playhead[CURRENT];

    int j;
    jack_nframes_t fill_last_nframes_in_pv;
    for(j=0; j<pat->npatternvoices; j++) {
      fill_last_nframes_in_pv = fill_last_nframes;
      qn_patternvoice_t* pv = pat->patternvoices + j;

      int pv_len = pv->nbeats * this_beat_len;
      int pv_end = pv_len;

    accum_one_pv:
      if(evtset == PREFIX) {
	qn_patternvoice_t *current = run->curr->longest_pv;
	pv_end = 0;

	jack_nframes_t longest_len = current->nbeats * ((run->sample_rate * 60) / (get_effective_bpm(run->curr,run->base_beats_per_minute)));
	int longest_playhead = current->playhead[CURRENT];

	pv->playhead_tmp[PREFIX] = longest_playhead - longest_len;
      } else if(evtset == SUFFIX) {
	if(run->prev) {
	  qn_patternvoice_t *current = run->curr->longest_pv;
	  jack_nframes_t longest_len = run->prev->longest_pv->nbeats *
	    ((run->sample_rate * 60) /
	     (get_effective_bpm(run->prev,run->base_beats_per_minute)));

	  int longest_playhead = current->playhead[CURRENT];
	  pv_end = pv_len + longest_playhead;

	setting_suffix_playheadtmp:
	  pv->playhead_tmp[SUFFIX] = longest_len + longest_playhead;
	} else {
	  // Nothing to do
	  return;
	}
      }

      int range_start = pv->playhead_tmp[evtset];
      int range_end = (evtset==PREFIX) ? 0 :
	range_start + fill_last_nframes;

      int maybe_more_loops = 1;
      while(maybe_more_loops) {
	if(pv->voice->port == port) {
	  int k;
	  for(k=0; k<pv->nevents; k++) {
	    qn_event_t ev = pv->events[k];

	    // evtime is expressing a value as a frame index.
	    // However, jack_nframes_t is unsigned and we need to
	    // support negative indices (prefixes).
	    int evtime = (this_beat_len * ev.start_tick)
	      / pat->tbm_ticks_per_beat;

	  looping_event_candidates:

	    // Enforce some space between note off and subsequent
	    // note on, otherwise the later note may not sound
	    // articulated
#           define NOTE_OFF_EARLY_NFRAMES (this_beat_len/8)
	    if((ev.data[0] & 0xf0) == 0x80) {
	      evtime -= NOTE_OFF_EARLY_NFRAMES;
	      if (evtime < 0 && evtset != PREFIX) {
		evtime += pv_len;
	      }
	    }

#           define MAX_OUT_EVENTS 1024
	    if(evtime >= range_start
	       && evtime < range_end
	       && *nout < MAX_OUT_EVENTS) {

	      qn_outevent_t* outev = out + *nout;

	      // time in the out event must be indexed
	      // with 0 being the start of nframes
	      outev->at = evtime - pv->playhead_tmp[evtset]
		+ (fill_last_nframes_orig - fill_last_nframes_in_pv);

	      outev->data[0] = ev.data[0];
	      outev->data[1] = ev.data[1];
	      outev->data[2] = ev.data[2];

	      switch(ev.data[0] & 0xf0) {
	      case 0x80:  // note off
	      case 0x90:  // note on
	      case 0xb0:  // control change
		outev->szdata = 3;
		outev->data[0] |= pv->voice->channel;
		break;
	      case 0xc0:  // program change
		outev->szdata = 2;
		outev->data[0] |= pv->voice->channel;
		break;
	      }

	    increment_nout:
	      *nout = *nout+1;
	    }
	  } // loop events
	} // if(pv->voice->port == port)

      inner_loop_end:
	if(range_end >= pv_end && evtset==CURRENT) {
	  // We are straddling the boundary of this loop and something.

	  if(fill_last_nframes >= til_curr_pattern_end) {
	    // This is the end of the pattern, and
	    // We're straddling two patterns
	    maybe_more_loops = 0;
	    *pattern_advanced = pattern_advanced_now = 1;
	    fill_last_nframes_in_pv = range_end - pv_end;
	  } else {
	    // Loop this patternvoice because there's
	    // still a longer one in progress
	    range_start = pv_end - pv_len;
	    range_end -= pv_end;
	    pv->playhead_tmp[evtset] = range_start;
	    fill_last_nframes_in_pv = range_end;
	  }

	} else {
	  // Simple case, this patternvoice didn't wrap
	  maybe_more_loops = 0;
	pv_no_wrap:
	  fill_last_nframes_in_pv = 0;
	  if(evtset==CURRENT) {
	    pv->playhead_tmp[evtset] = range_end;
	  }
	}
      } // while(maybe_more_loops)
    } // loop patternvoices

    if(*pattern_advanced && evtset == CURRENT) {
      int playhead_target;
      if(pattern_advanced_now) {
	pat = ctx->next_pattern_func(ctx);
	playhead_target = 0;
      } else {
	playhead_target = fill_last_nframes;
      }
    
      if(pat) {
	for(k=0;k<pat->npatternvoices;k++) {
	  qn_patternvoice_t* pv = pat->patternvoices + k;
	  pv->playhead_tmp[evtset] = playhead_target;
	}
      }
    }

  save_out_nframes:
    fill_last_nframes = fill_last_nframes_in_pv;

  } // while(fill_last_nframes)
}

int process(jack_nframes_t nframes, void *arg) {
  qn_context_t* ctx = (qn_context_t*)arg;
  qn_runstate_t* run = ctx->run;

  // Clear all port buffers.  Jack doesn't clear them
  // between calls
  int i;
  for(i=0; i<ctx->config->noutports; i++) {
    qn_port_t* port = run->ports[i];
    void* port_buf = jack_port_get_buffer( port->jackport, nframes);
    jack_midi_clear_buffer(port_buf);
  }

  jack_transport_state_t transport = jack_transport_query(run->client, NULL);

  // If a change in transport state
  if(transport != run->transtate) {
    run->transtate = transport;

    // If stopping, send "All Notes Off" MIDI message
    if(transport == JackTransportStopped) {
      for(i=0; i<ctx->config->noutports; i++) {
	qn_port_t* port = run->ports[i];

	int channels_encountered[16];
	int ce;
	for(ce=0;ce<16;ce++) {
	  channels_encountered[ce] = 0;
	}

	void* port_buf = jack_port_get_buffer( port->jackport, nframes);
	jack_midi_clear_buffer(port_buf);

	int j;
	unsigned char val[NBYTES_MIDI];
	if(port->clock_numerator) { // if port is receiving clock messages
	  val[0] = 0xfc; // clock stop
	  if(jack_midi_event_write(port_buf,0,val,1)) {
	    printf("Couldn't reserve space for a clock stop event.\n");
	  }
	}

	for(j=0; j<ctx->config->nvoices; j++) {
	  qn_voice_t* voice = ctx->config->voices + j;
	  if(voice->port == port) {

	    // Don't write multiple messages per-channel per-port
	    if(channels_encountered[voice->channel]) continue;

	    channels_encountered[voice->channel] = 1;

	    val[0] = (176 | voice->channel);
	    val[1] = 123;
	    val[2] = 0;
	    if(jack_midi_event_write(port_buf,0,val,NBYTES_MIDI)) {
	      printf("Couldn't reserve space for a note off event.\n");
	    }
	  }
	}
	
      }
    }
  }

  if(!run->curr) return 0;

  int pattern_advanced = 0;
  int start_message_sent = 0;

  // Write a list of output events, grouped by output port
  for(i=0; i<ctx->config->noutports; i++) {
    qn_port_t* port = run->ports[i];

    // We can't write events to the actual port_buf yet since they
    // would be out of order.  Use a fixed buffer for our intermediate
    // event store.
    int nout = 0;
    qn_outevent_t out[MAX_OUT_EVENTS];

  pre_accum_events:
    accumulate_clock_events(ctx,port,&nout,out,nframes,&start_message_sent);

    if(transport == JackTransportRolling && !run->awaiting_common_start_beat) {
      accumulate_pattern_events(ctx,port,ctx->next_pattern_func(ctx),
				&nout,out,nframes,&pattern_advanced, PREFIX);
      accumulate_pattern_events(ctx,port,run->prev,
				&nout,out,nframes,&pattern_advanced, SUFFIX);
      accumulate_pattern_events(ctx,port,run->curr,
				&nout,out,nframes,&pattern_advanced, CURRENT);
    }

    if(nout) {

      qsort(out,nout,sizeof(qn_outevent_t),cmp_outevent);

      void* port_buf = jack_port_get_buffer( port->jackport, nframes);
      jack_midi_clear_buffer(port_buf);

      jack_midi_data_t* buffer;
      int j;
      for(j=0; j<nout; j++) {
	qn_outevent_t* evt = out + j;

      reserve_out_event:
	buffer = jack_midi_event_reserve(port_buf, evt->at, evt->szdata);
	if(buffer) {
	  memcpy(buffer,evt->data,evt->szdata);
	} else {
	  printf("Couldn't send event, probably the time %d is wrong.\n",
		 evt->at);
	}
      }
    }
  } // loop noutports

  if(transport == JackTransportRolling && !run->awaiting_common_start_beat) {
    set_playheads(ctx,pattern_advanced,PREFIX);
    set_playheads(ctx,pattern_advanced,SUFFIX);
    set_playheads(ctx,pattern_advanced,CURRENT);

    // Calculate bar/beat/tick since we are a jack timebase master.
    // It's easier to do this before we advance the pattern than
    // to do it in timebase() where we have to look backwards.
    jack_nframes_t frames_left = nframes;
    qn_pattern_t* pat = run->curr;
    while(frames_left && pat) {
      int bpm = get_effective_bpm(pat,run->base_beats_per_minute);
      jack_nframes_t tick_len = (run->sample_rate * 60) / (bpm * pat->tbm_ticks_per_beat);

      run->frame_in_tick += frames_left;
      if(run->frame_in_tick > tick_len) {
	// We advanced a tick
	run->tbm_bar_start_tick++;
	run->tbm_tick++;
	if(run->tbm_tick > pat->tbm_ticks_per_beat) {
	  // We advanced a beat
	  run->tbm_tick = 1;
	  run->tbm_beat++;

	  if(run->tbm_beat > pat->tbm_beats_per_bar) {
	    // We advanced a bar
	    run->tbm_bar_start_tick = 1;
	    run->tbm_beat = 1;
	    run->tbm_bar++;

	    if(run->tbm_bar*pat->tbm_beats_per_bar > pat->longest_pv->nbeats) {
	      // We advanced to the next pattern
	      pat = ctx->next_pattern_func(ctx);
	    }
	  }
	}

	frames_left -= (run->frame_in_tick - tick_len);
	run->frame_in_tick = 0;
      } else {
	// still in same tick
	frames_left = 0;
      }
    }
    // End BBT calculations

    if(pattern_advanced) {
      run->prev = run->curr;
      run->curr = ctx->next_pattern_func(ctx);

      if(run->curr && run->curr->bpm_relation == Absolute) {
	run->base_beats_per_minute = run->curr->beats_per_minute;
      }
    }
  } //     if(transport == JackTransportRolling) 

  return 0;
}

void timebase(jack_transport_state_t state,
	      jack_nframes_t nframes,
	      jack_position_t *pos,
	      int new_pos,
	      void *arg) {

  pos->valid = (JackPositionBBT | JackBBTFrameOffset);

  qn_context_t* ctx = (qn_context_t*)arg;

  if(new_pos && pos->frame==0) {
    // Set the song time
    qn_reset_playhead(ctx);
  } else {
    // Report the song time
    pos->frame = ctx->run->elapsed;
  }

  qn_runstate_t* run = ctx->run;
  // BBT fields
  pos->bar = run->tbm_bar;
  pos->beat = run->tbm_beat;
  pos->tick = run->tbm_tick;
  pos->bar_start_tick = run->tbm_bar_start_tick;
  pos->bbt_offset = run->frame_in_tick;
  if(run->curr) {
    pos->beats_per_bar = run->curr->tbm_beats_per_bar;
    pos->beat_type = run->curr->tbm_beat_type;
    pos->ticks_per_beat = run->curr->tbm_ticks_per_beat;
    // TODO: set bpm to interpolated value as per transport.h
    pos->beats_per_minute = get_effective_bpm(run->curr,run->base_beats_per_minute);
  }
}
