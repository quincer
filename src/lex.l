%option nodefault warn yylineno

%{
#include <limits.h>
#include "lex.h"
#define MAX_FILENAME_LEN 64

  int comment_caller;

  qn_config_t* config = NULL;
  char filename[MAX_FILENAME_LEN+1];

  static void start_include(int quoted) {
    memset(filename,0,MAX_FILENAME_LEN);
    char* start = yytext;
    int len = strlen(start);
    if(quoted) {
      start += 1;
      len -= 2;
    }
    strncpy(filename,start,
	    (len > MAX_FILENAME_LEN) ? MAX_FILENAME_LEN : len);
    // fprintf(stderr,"Just lexed a filename: %s\n",filename);

    yyin = fopen(filename,"r");
    if(!yyin) {
      fprintf(stderr,"Couldn't open include file %s\n",filename);
      exit(2);
    }
    yypush_buffer_state(yy_create_buffer(yyin,YY_BUF_SIZE));
  }

  // TODO: other channel mode messages:
  // aftertouch (both kinds), program change, pitch bend
  enum EventType {
    NoneYet = -1,
    NoteOn,
    ControlChange,
    ProgramChange
  };

  struct eventdef {
    int is_set;
    enum EventType type;
    char data[NBYTES_MIDI-1];
  };

# define MAX_EVENT_DEFS 128
  struct eventdef eventdefs[MAX_EVENT_DEFS];

  enum EventType last_event_type = NoneYet;
  char current_eventdef = '\0';

  int passed_prefix, prefix_len,
    beats_per_bar_known, ticks_per_beat_known,
    passed_primary_content, primary_len, suffix_len;

  int currtick;
  unsigned char curreventdef = '\0';

  /* When lexing a line of events for a pv, we won't know which voice
     the events are for until the end of the line, so we buffer them
     until then.  This allows multiple lines to be specified per voice
     (polyphony, automation, etc) */
# define MAX_LINE_EVENTS 1024
  int nevents = 0;
  qn_event_t events[MAX_LINE_EVENTS];
  /* int note_wraps_pv = 0; */

  static void maybe_note_off() {
    // Maybe emit a note-off if prior was note-on.
    // TODO: if last tick is EVENTDEFCHAR, be sure to add the note off there too.
    if(curreventdef
       && eventdefs[curreventdef].is_set
       && eventdefs[curreventdef].type==NoteOn) {

      nevents++;
      if(nevents > MAX_LINE_EVENTS) {
	fprintf(stderr,"Too many events defined in one line (> %d), char '%c' at line %d\n",
		MAX_LINE_EVENTS, curreventdef, yylineno);
	exit(2);
      }

      qn_event_t* evtnew = events + nevents - 1;
      evtnew->start_tick = currtick;
      evtnew->data[0] = 0x80; // note off
      evtnew->data[1] = eventdefs[curreventdef].data[0];

      curreventdef = '\0';
    }
  }

  int current_clock_port_index = -1;
  static int ensure_port(char* name, char** permname) {
    int i;
    for(i=0; i<config->noutports; i++) {
      char* thisp = config->outports[i].name;
      if(!strcmp(thisp,name)) {
	*permname = thisp;
	return i;
      }
    }

    config->noutports++;
    config->outports = realloc(config->outports, sizeof(qn_config_port_t) * config->noutports);
    config->outports[config->noutports-1].name = *permname = strndup(name,strlen(yytext));
    config->outports[config->noutports-1].clock_numerator = 0;
    config->outports[config->noutports-1].clock_denominator = 0;
    return config->noutports-1;
  }

  /* Get the MIDI pitch if this was in octave four according to
     https://en.wikipedia.org/wiki/Scientific_pitch_notation
  */
  static char get_octave_four_pitch(char* token) {
    size_t len = strnlen(token,4);
    int is_sharp = (len > 1) && token[1]=='#';
    int is_flat = (len > 1) && token[1]=='b';

    if(is_sharp) {
      switch(token[0]) {
      case 'a':
      case 'A':
	return 70;
      case 'b':
      case 'B':
	return 60;
      case 'c':
      case 'C':
	return 61;
      case 'd':
      case 'D':
	return 63;
      case 'e':
      case 'E':
	return 65;
      case 'f':
      case 'F':
	return 66;
      case 'g':
      case 'G':
	return 68;
      }
    } else if(is_flat) {
      switch(token[0]) {
      case 'a':
      case 'A':
	return 68;
      case 'b':
      case 'B':
	return 70;
      case 'c':
      case 'C':
	return 71;
      case 'd':
      case 'D':
	return 61;
      case 'e':
      case 'E':
	return 63;
      case 'f':
      case 'F':
	return 64;
      case 'g':
      case 'G':
	return 66;
      }
    } else {
      switch(token[0]) {
      case 'a':
      case 'A':
	return 69;
      case 'b':
      case 'B':
	return 71;
      case 'c':
      case 'C':
	return 60;
      case 'd':
      case 'D':
	return 62;
      case 'e':
      case 'E':
	return 64;
      case 'f':
      case 'F':
	return 65;
      case 'g':
      case 'G':
	return 67;
      }
    }

    // Shouldn't happen since token regex should only match correct vals
    return -1;
  }

  static char midi_pitch_from_named_absolute(char* token) {
    // midi note range:  c-1 (0) to g9 (127)
    size_t len = strnlen(token,4);
    char octave = token[len-1] - '0';
    if(token[len-2]=='-') octave = -octave;
    
    char o4pitch = get_octave_four_pitch(token);
    if(o4pitch < 0) return o4pitch;

    char newpitch = o4pitch + (12 * (octave-4));

    if(newpitch < 0 || newpitch > 127) return -1;

    return newpitch;
  }

  // Initial value is F4 so that if the first event def
  // has a relative pitch it will end up in octave 4
  char last_midi_pitch = 65;

  static char midi_pitch_from_named_relative(char* token) {
    // midi note range:  c-1 (0) to g9 (127)

    char o4pitch = get_octave_four_pitch(token);
    if(o4pitch < 0) return o4pitch;

    char test_pitch = o4pitch;
    if(test_pitch >= last_midi_pitch) {
      while(test_pitch - last_midi_pitch > 6 && test_pitch >= 12) {
	test_pitch -= 12;
      }
      return test_pitch;
    } else { // test_pitch < last_midi_pitch
      while(last_midi_pitch - test_pitch >= 6
	    && CHAR_MAX-12 >= test_pitch) {
	test_pitch += 12;
      }
      return test_pitch;
    }
  }

%}

%s voicename voiceport voicechannel voicenl
%s incfilename
%s comment
%s patname patbpm patswing
%s eventdefchar eventdefcharortickdef eventtypeorval eventval1 eventval1progchange eventval2
%s tickdefchar
%s pv pvvoicename
%s runspec
%s clockport clocknum clockden clocknl

ID [a-z][a-z0-9]{0,15}
CHANNEL 1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16

EVENTDEFCHAR [[:print:]]{-}[S0\.\'|\- ]
EVENTTYPE note|cc|pc
EVENTDEFSHORT [0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-7]
RUNSPEC once|loop

NAMEDPITCHABSOLUTE [a-gA-G][b#]?(-1|[0-9])
NAMEDPITCHRELATIVE [a-gA-G][b#]?

%%
<INITIAL,pv>voice BEGIN(voicename);

<INITIAL>run BEGIN(runspec);

<runspec>{RUNSPEC} {
   if(!strcmp("once",yytext)) {
     config->next_pattern_func = once_through;
   } else if(!strcmp("loop",yytext)) {
     config->next_pattern_func = loop_all;
   } else {
      fprintf(stderr,"Invalid run spec %s at line %d\n", yytext, yylineno);
      exit(2);
   }
   BEGIN(INITIAL);
 }

<voicename>{ID} {
  int i;
  for(i=0; i<config->nvoices; i++) {
    qn_voice_t* thisv = config->voices + i;
    if(!strcmp(thisv->name,yytext)) {
      fprintf(stderr,"Duplicate declaration of voice %s at line %d\n", yytext, yylineno);
      exit(2);
    }
  }

  config->nvoices++;
  config->voices = realloc(config->voices, sizeof(qn_voice_t) * config->nvoices);
  config->voices[config->nvoices-1].name = strndup(yytext,strlen(yytext));
  BEGIN(voiceport);
 }

<voiceport>{ID} {
  char* permname;
  int portindex = ensure_port(yytext, &permname);

  config->voices[config->nvoices-1].portname = permname;
  BEGIN(voicechannel);
 }

<voicechannel>{CHANNEL} {
  // -1 since channels in input are 1-based but in raw data are
  // -0-based
  config->voices[config->nvoices-1].channel = atoi(yytext)-1;
  BEGIN(voicenl);
 }

<voicenl>\n BEGIN(INITIAL);

<INITIAL,pv>clock BEGIN(clockport);

<clockport>{ID} {
  char* permname;
  current_clock_port_index = ensure_port(yytext, &permname);
  BEGIN(clocknum);
 }

<clocknum>[1-9][0-9]* {
  config->outports[current_clock_port_index].clock_numerator = atoi(yytext);
  BEGIN(clockden);
 }

<clockden>[1-9][0-9]* {
  config->outports[current_clock_port_index].clock_denominator = atoi(yytext);
  current_clock_port_index = -1;
  BEGIN(clocknl);
 }

<clocknl>\n BEGIN(INITIAL);

<INITIAL,pv>include BEGIN(incfilename);

<incfilename>\".*\" {
  /* Allow quotes around a filename so that it can have spaces. */
  start_include(1);
  BEGIN(INITIAL);
 }

<incfilename>[^ \t\n]+ {
  /* Copy an unquoted filename */
  start_include(0);
  BEGIN(INITIAL);
 }

# {
  comment_caller = YY_START;
  BEGIN(comment);
}
<comment>\n {
  unput('\n');
  BEGIN(comment_caller);
}
<comment>.* 

<INITIAL,pv>pattern {
  config->npatterns++;
  config->patterns = realloc(config->patterns, sizeof(qn_pattern_t) * config->npatterns);
  qn_pattern_t* newp = config->patterns + config->npatterns - 1;
  newp->npatternvoices = 0;
  newp->patternvoices = NULL;
  newp->swing_value = 0.0;
  newp->tbm_ticks_per_beat = 0;
  newp->tbm_beats_per_bar = 0.0;
  newp->tbm_beat_type = 4;
  newp->beats_per_minute = 0;
  newp->bpm_relation = Absolute;

  passed_prefix = prefix_len = 0;
  passed_primary_content = primary_len = suffix_len = 0;
  ticks_per_beat_known = 0;
  beats_per_bar_known = 0;

  BEGIN(patbpm);
 }

<patbpm>[+-]?[123]?[0-9]{1,2} {
  qn_pattern_t* newp = config->patterns + config->npatterns - 1;
  if(*yytext == '+' || *yytext == '-') {
    newp->bpm_relation = RelativeToBase;
  }
  newp->beats_per_minute = atof(yytext);
  BEGIN(patswing);
 }

<patswing>[01]?\.?[0-9]+ {
  qn_pattern_t* newp = config->patterns + config->npatterns - 1;
  newp->swing_value = atof(yytext);

  // initialize note definitions
  int i;
  for(i=0; i<MAX_EVENT_DEFS; i++) {
    eventdefs[i].is_set = 0;
  }
  last_event_type = NoneYet;
  current_eventdef = '\0';
  BEGIN(eventdefchar);
 }

<eventdefchar,eventdefcharortickdef>{EVENTDEFCHAR} {
  current_eventdef = *yytext;
  struct eventdef* def = eventdefs + current_eventdef;

  if(def->is_set) {
    fprintf(stderr,"Duplicate declaration of note definition %s at line %d\n", yytext, yylineno);
    exit(2);
  }

  def->is_set = 1;

  BEGIN(eventtypeorval);
 }

<eventtypeorval>{EVENTTYPE} {
  struct eventdef* def = eventdefs + current_eventdef;
  if(!strcmp("note",yytext)) {
    def->type = last_event_type = NoteOn;
    BEGIN(eventval1);
  } else if(!strcmp("cc",yytext)) {
    def->type = last_event_type = ControlChange;
    BEGIN(eventval1);
  } else if(!strcmp("pc",yytext)) {
    def->type = last_event_type = ProgramChange;
    BEGIN(eventval1progchange);
  }
 }

<eventtypeorval,eventval1>{NAMEDPITCHABSOLUTE} {
  struct eventdef* def = eventdefs + current_eventdef;

  // Default to note event if none was specified
  if(last_event_type==NoneYet) {
    def->type = last_event_type = NoteOn;
  }

  char parse_result = midi_pitch_from_named_absolute(yytext);
  if(parse_result == -1) {
    fprintf(stderr,"Invalid absolute named pitch token %s at line %d\n", yytext, yylineno);
    exit(2);
  }
  
  def->data[0] = last_midi_pitch = parse_result;
  BEGIN(eventval2);
 }

<eventtypeorval,eventval1>{NAMEDPITCHRELATIVE} {
  struct eventdef* def = eventdefs + current_eventdef;

  // Default to note event if none was specified
  if(last_event_type==NoneYet) {
    def->type = last_event_type = NoteOn;
  }

  char parse_result = midi_pitch_from_named_relative(yytext);
  if(parse_result == -1) {
    fprintf(stderr,"Invalid relative named pitch token %s at line %d\n", yytext, yylineno);
    exit(2);
  }
  
  def->data[0] = last_midi_pitch = parse_result;
  BEGIN(eventval2);
 }

<eventtypeorval,eventval1>{EVENTDEFSHORT} {
  struct eventdef* def = eventdefs + current_eventdef;

  // Default to note event if none was specified
  if(last_event_type==NoneYet) {
    def->type = last_event_type = NoteOn;
  }

  def->data[0] = last_midi_pitch = (char)atoi(yytext);
  BEGIN(eventval2);
 }

<eventval1progchange>{EVENTDEFSHORT} {
  struct eventdef* def = eventdefs + current_eventdef;
  def->data[0] = (char)atoi(yytext);
  BEGIN(eventdefcharortickdef);
 }

<eventval2>{EVENTDEFSHORT} {
  struct eventdef* def = eventdefs + current_eventdef;
  def->data[1] = (char)atoi(yytext);
  BEGIN(eventdefcharortickdef);
 }

<eventdefcharortickdef,tickdefchar>\| {
  if(passed_prefix) {
    beats_per_bar_known = 1;
    ticks_per_beat_known = 1;
    if(passed_primary_content) {
      suffix_len++;
    } else {
      primary_len++;
    }
  } else {
    prefix_len++;
  }
  
  BEGIN(tickdefchar);
 }

<eventdefcharortickdef,tickdefchar>0 {
  qn_pattern_t* newp = config->patterns + config->npatterns - 1;
  if(newp->tbm_beats_per_bar == 0.0) {
    newp->tbm_beats_per_bar = 1.0;
    newp->tbm_ticks_per_beat++;
  }
  primary_len = 1;
  passed_prefix = 1;
  BEGIN(tickdefchar);
 }

<tickdefchar>S {
  passed_primary_content = 1;
  suffix_len++;
 }

<eventdefcharortickdef,tickdefchar>\x27 { // A single quote '
  if(passed_prefix) {
    qn_pattern_t* newp = config->patterns + config->npatterns - 1;
    if(!beats_per_bar_known) {
      newp->tbm_beats_per_bar += 1.0;
    }
    ticks_per_beat_known = 1;
    if(passed_primary_content) {
      suffix_len++;
    } else {
      primary_len++;
    }
  } else {
    prefix_len++;
  }
  BEGIN(tickdefchar);
}

<eventdefcharortickdef,tickdefchar>\. {
  if(passed_prefix) {
    qn_pattern_t* newp = config->patterns + config->npatterns - 1;
    if(!ticks_per_beat_known) {
      newp->tbm_ticks_per_beat++;
    }
    if(passed_primary_content) {
      suffix_len++;
    } else {
      primary_len++;
    }
  } else {
    prefix_len++;
  }
  BEGIN(tickdefchar);
 }

<tickdefchar>\n {
  currtick = -prefix_len;
  BEGIN(pv);
 }

<pv>{EVENTDEFCHAR} {
  maybe_note_off();

  curreventdef = *yytext;
  struct eventdef* def = eventdefs + curreventdef;
  if(!def->is_set) {
    fprintf(stderr,"Unknown event def char '%c' at line %d\n", curreventdef, yylineno);
    exit(2);
  }

  nevents++;
  if(nevents > MAX_LINE_EVENTS) {
    fprintf(stderr,"Too many events defined in one line (> %d), char '%c' at line %d\n",
	    MAX_LINE_EVENTS, curreventdef, yylineno);
    exit(2);
  }
  qn_event_t* evtnew = events + nevents - 1;
  evtnew->start_tick = currtick;
  switch(def->type) {
  case NoteOn:
    evtnew->data[0] = 0x90; // note on
    evtnew->data[1] = def->data[0];
    evtnew->data[2] = def->data[1];
    break;
  case ControlChange:
    evtnew->data[0] = 0xB0; // control change
    evtnew->data[1] = def->data[0];
    evtnew->data[2] = def->data[1];
    break;
  case ProgramChange:
    evtnew->data[0] = 0xC0; // program change
    evtnew->data[1] = def->data[0] - 1; // program 1 should get 0
    evtnew->data[2] = 0;
    break;
  case NoneYet:
    break;
  }

  currtick++;
 }

<pv>" " {
  maybe_note_off();
  currtick++;
 }

<pv>- {
  currtick++;
 }

<pv>\| {
  maybe_note_off();
  BEGIN(pvvoicename);
 }

<pvvoicename>{ID} {
  // match to valid voice
  qn_voice_t* voice = NULL;
  int i;
  for(i=0;i<config->nvoices;i++) {
    qn_voice_t* onevoice = config->voices+i;
    if(!strcmp(onevoice->name,yytext)) {
      voice = onevoice;
    }
  }

  if(!voice) {
    fprintf(stderr,"Line %d must match to a valid voice.  No voice %s exists.\n",yylineno,yytext);
    exit(2);
  }

  qn_pattern_t* newp = config->patterns + config->npatterns - 1;
  if(!newp->tbm_ticks_per_beat) {
    fprintf(stderr,"Unable to deduce ticks per beat.  Does your meter line contain a 0-mark?.\n");
    exit(2);
  }

  newp->npatternvoices++;
  newp->patternvoices =
    realloc(newp->patternvoices,sizeof(qn_patternvoice_t) * newp->npatternvoices);

  qn_patternvoice_t* thispv = newp->patternvoices + (newp->npatternvoices - 1);
  thispv->events = NULL;
  thispv->nevents = 0;
  thispv->voice = voice;
  thispv->nbeats = ((currtick>primary_len) ? primary_len : currtick) / newp->tbm_ticks_per_beat;

  // copy event buffer to pv
  thispv->events =
    realloc(thispv->events,sizeof(qn_event_t)*(thispv->nevents + nevents));
  memcpy(thispv->events + thispv->nevents, events, nevents*sizeof(qn_event_t));
  thispv->nevents += nevents;

  // TODO: sort events by tick then event type?

  // reset event buffer
  nevents = 0;
  curreventdef = '\0';
  currtick = -prefix_len;
  BEGIN(pv);
 }

[ \t\n] /* ignore whitespace */

. { fprintf(stderr,"Bad input character '%s' at line %d\n", yytext, yylineno);
  exit(2);
 }

<<EOF>> {
  yypop_buffer_state();
  if ( !YY_CURRENT_BUFFER ) {
    yyterminate();
  }
  }

%%
int yywrap() { return 1; }

qn_config_t* parse_config(char* filename) {
  yyin = fopen(filename,"r");
  config = malloc(sizeof(qn_config_t));
  qn_init_config(config);
  if(yylex()) {
    qn_free_config(config);
    free(config);
    config = NULL;
  }
  fclose(yyin);
  yylex_destroy();
  return config;
}
