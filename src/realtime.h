#ifndef __realtime_h__
#define __realtime_h__

int process(jack_nframes_t nframes, void *arg);

void timebase(jack_transport_state_t state,
	      jack_nframes_t nframes,
	      jack_position_t *pos,
	      int new_pos,
	      void *arg);

#endif /* __realtime_h__ */
