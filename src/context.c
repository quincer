#include "context.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

qn_pattern_t* loop_one(void* ctx) {
  qn_context_t* real_ctx = ctx;
  return real_ctx->run->curr;
}

static qn_pattern_t* advance_pattern(void* ctx, int loop) {
  qn_context_t* real_ctx = ctx;
  qn_config_t* config = real_ctx->config;
  int i;
  for(i=0; i<config->npatterns; i++) {
    qn_pattern_t* pat = config->patterns + i;
    if(pat == real_ctx->run->curr) {
      int patindex = i+1;
      if(patindex==config->npatterns) {
	if(loop) {
	  patindex = 0;
	} else {
	  break;
	}
      }
      return config->patterns + patindex;
    }
  }
  return NULL;
}
qn_pattern_t* loop_all(void* ctx) {
  return advance_pattern(ctx,1);
}
qn_pattern_t* once_through(void* ctx) {
  return advance_pattern(ctx,0);
}

qn_config_t* qn_get_fake_config() {
  qn_config_t* config = malloc(sizeof(qn_config_t));
  qn_init_config(config);

  config->noutports = 1;
  char* portname = "fluidsynth";
  config->outports = malloc(sizeof(qn_config_port_t) * config->noutports);
  config->outports[0].name = strndup(portname,strlen(portname));
  config->outports[0].clock_numerator = 0;
  config->outports[0].clock_denominator = 0;

  config->nvoices = 1;
  config->voices = malloc(sizeof(qn_voice_t) * config->nvoices);
  char* voicename = "bass";
  config->voices[0].name = strndup(voicename,strlen(voicename));
  int i;
  for(i=0; i<config->noutports; i++) {
    /* fprintf(stderr,"First, does %s equal %s\n","fluidsynth",config->outports[i]); */
    if(!strcmp("fluidsynth",config->outports[i].name)) {
      config->voices[0].portname = config->outports[i].name;
      break;
    }
  }
  config->voices[0].channel = 14;
  config->voices[0].port = NULL;

  config->npatterns = 2;
  config->patterns = malloc(sizeof(qn_pattern_t) * config->npatterns);

  qn_pattern_t* p;
  p = config->patterns;
  p->npatternvoices = 2;
  p->patternvoices = malloc(sizeof(qn_patternvoice_t) * p->npatternvoices);
  p->beats_per_minute = 144;
  p->bpm_relation = Absolute;
  p->tbm_ticks_per_beat = 2;
  p->tbm_beats_per_bar = 4;
  p->tbm_beat_type = 4;

  qn_patternvoice_t* pv = p->patternvoices + 0;
  pv->voice = config->voices + 0;
  pv->nbeats = 4;
  pv->nevents = 2;
  pv->events = malloc(sizeof(qn_event_t) * pv->nevents);
  pv->events[0].start_tick = 0;
  pv->events[0].data[0] = 0x90; // note on
  pv->events[0].data[1] = 60; // pitch
  pv->events[0].data[2] = 110; // vel
  pv->events[1].start_tick = 6;
  pv->events[1].data[0] = 0x80; // note off
  pv->events[1].data[1] = 60; // pitch

  pv = p->patternvoices + 1;
  pv->voice = config->voices + 0;
  pv->nbeats = 1;
  pv->nevents = 2;
  pv->events = malloc(sizeof(qn_event_t) * pv->nevents);
  pv->events[0].start_tick = 0;
  pv->events[0].data[0] = 0x90; // note on
  pv->events[0].data[1] = 53; // pitch
  pv->events[0].data[2] = 64; // vel
  pv->events[1].start_tick = 1;
  pv->events[1].data[0] = 0x80; // note off
  pv->events[1].data[1] = 53; // pitch



  p = config->patterns + 1;
  p->npatternvoices = 1;
  p->patternvoices = malloc(sizeof(qn_patternvoice_t) * p->npatternvoices);
  p->beats_per_minute = 56;
  p->tbm_ticks_per_beat = 2;
  p->tbm_beats_per_bar = 4;
  p->tbm_beat_type = 4;
  p->bpm_relation = RelativeToBase;

  pv = p->patternvoices + 0;
  pv->voice = config->voices + 0;
  pv->nbeats = 4;
  pv->nevents = 12;
  pv->events = malloc(sizeof(qn_event_t) * pv->nevents);
  pv->events[0].start_tick = 0;
  pv->events[0].data[0] = 0x90; // note on
  pv->events[0].data[1] = 58; // pitch
  pv->events[0].data[2] = 64; // vel
  pv->events[1].start_tick = 1;
  pv->events[1].data[0] = 0x80; // note off
  pv->events[1].data[1] = 58; // pitch

  pv->events[2].start_tick = 0;
  pv->events[2].data[0] = 0x90; // note on
  pv->events[2].data[1] = 67; // pitch
  pv->events[2].data[2] = 64; // vel
  pv->events[3].start_tick = 2;
  pv->events[3].data[0] = 0x80; // note off
  pv->events[3].data[1] = 67; // pitch

  pv->events[4].start_tick = 2;
  pv->events[4].data[0] = 0x90; // note on
  pv->events[4].data[1] = 58; // pitch
  pv->events[4].data[2] = 64; // vel
  pv->events[5].start_tick = 3;
  pv->events[5].data[0] = 0x80; // note off
  pv->events[5].data[1] = 58; // pitch


  pv->events[6].start_tick = 4;
  pv->events[6].data[0] = 0x90; // note on
  pv->events[6].data[1] = 59; // pitch
  pv->events[6].data[2] = 64; // vel
  pv->events[7].start_tick = 5;
  pv->events[7].data[0] = 0x80; // note off
  pv->events[7].data[1] = 59; // pitch

  pv->events[8].start_tick = 4;
  pv->events[8].data[0] = 0x90; // note on
  pv->events[8].data[1] = 67; // pitch
  pv->events[8].data[2] = 64; // vel
  pv->events[9].start_tick = 6;
  pv->events[9].data[0] = 0x80; // note off
  pv->events[9].data[1] = 67; // pitch

  pv->events[10].start_tick = 6;
  pv->events[10].data[0] = 0x90; // note on
  pv->events[10].data[1] = 59; // pitch
  pv->events[10].data[2] = 64; // vel
  pv->events[11].start_tick = 7;
  pv->events[11].data[0] = 0x80; // note off
  pv->events[11].data[1] = 59; // pitch

  config->next_pattern_func = loop_all;
  
  return config;
}

void qn_init_config(qn_config_t* config) {
  config->nvoices = 0;
  config->voices = NULL;
  config->noutports = 0;
  config->outports = NULL;
  config->npatterns = 0;
  config->patterns = NULL;
  config->next_pattern_func = NULL;
}

qn_context_t* qn_init_context(qn_config_t* config, jack_client_t* client) {
  qn_context_t* ctx = malloc(sizeof(qn_context_t));
  ctx->config = config;

  ctx->run = malloc(sizeof(qn_runstate_t));
  ctx->run->client = client;
  ctx->run->ports = malloc(sizeof(qn_port_t*) * config->noutports);
  ctx->run->awaiting_common_start_beat = 0;
  int i;
  for(i=0; i<config->noutports; i++) {
    ctx->run->ports[i] = malloc(sizeof(qn_port_t));
    ctx->run->ports[i]->jackport =
      jack_port_register(client, config->outports[i].name,
			 JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
    ctx->run->ports[i]->clock_numerator = config->outports[i].clock_numerator;
    /* printf("Set num to %d\n",ctx->run->ports[i]->clock_numerator); */
    ctx->run->ports[i]->clock_denominator = config->outports[i].clock_denominator;
    ctx->run->ports[i]->clock_next_tick = 0;
    ctx->run->ports[i]->clock_tick_index = 0;
  }

  // Set the port on each voice
  for(i=0; i<config->nvoices; i++) {
    int found = 0;
    int j;
    for(j=0; j<config->noutports; j++) {
      /* fprintf(stderr,"Does %s equal %s\n",config->outports[j],config->voices[i].portname); */
      if(!strcmp(config->outports[j].name,config->voices[i].portname)) {
	config->voices[i].port = ctx->run->ports[j];
	found = 1;
	break;
      }
    }
    if(!found) {
      fprintf(stderr,"No port was created for this voice\n");
    }
  }

  // Identify the longest patternvoice for each pattern
  for(i=0; i<config->npatterns; i++) {
    qn_pattern_t* pat = config->patterns + i;
    int j, max_beats = 0;
    for(j=0; j<pat->npatternvoices; j++) {
      qn_patternvoice_t* pv = pat->patternvoices + j;
      if(pv->nbeats > max_beats) {
	max_beats = pv->nbeats;
	pat->longest_pv = pv;
      }
    }
  }

  ctx->run->prev = NULL;
  ctx->run->curr = config->patterns; // TODO: don't just use first-parsed pattern

  ctx->run->sample_rate = jack_get_sample_rate(client);

  ctx->run->transtate = JackTransportStopped;

  if(ctx->run->curr->bpm_relation == RelativeToBase) {
    fprintf(stderr,"First pattern must have an absolute tempo.\n");
    exit(1);
  } else {
    ctx->run->base_beats_per_minute = ctx->run->curr->beats_per_minute;
  }

  ctx->next_pattern_func =
    (config->next_pattern_func ? config->next_pattern_func : loop_all);

  return ctx;
}

/* Resets all patterns to the beginning of their playhead.  May be
   called from realtime thread.

   Note that we don't change the "current" and "next" pattern pointers
   since controllers may set those, and therefore the first pattern
   may vary with each song start.  Exception: if "current" is NULL,
   the song is over and we have been requested to locate 0.

   For the same reason, the base BPM is also not reset.
*/
void qn_reset_playhead(qn_context_t* ctx) {
  qn_config_t* config = ctx->config;
  qn_runstate_t* run = ctx->run;

  run->elapsed = 0;

  int i;
  for(i=0; i<config->npatterns; i++) {
    qn_pattern_t* pat = config->patterns + i;
    int j;
    for(j=0; j<pat->npatternvoices; j++) {
      qn_patternvoice_t* pv = pat->patternvoices + j;
      int k;
      for(k=0;k<NEVENTSETS;k++) {
	pv->playhead[k] = 0;
	pv->playhead_tmp[k] = 0;
      }
    }
  }

  // Timebase master values
  run->tbm_bar = 1;
  run->tbm_beat = 1;
  run->tbm_tick = 1;
  run->tbm_bar_start_tick = 1;
  run->frame_in_tick = 0;

  // Song has finished, set back to start
  if(!run->curr) {
    run->prev = NULL;
    run->curr = config->patterns;
  }
}

void qn_free_config(qn_config_t* cfg) {
  int i;
  for(i=0; i<cfg->nvoices; i++) {
    cfg->voices[i].port = NULL;
    free(cfg->voices[i].name);
  }
  free(cfg->voices);

  for(i=0; i<cfg->noutports; i++) {
    free(cfg->outports[i].name);
  }
  free(cfg->outports);

  for(i=0; i<cfg->npatterns; i++) {
    qn_pattern_t* p = cfg->patterns + i;

    if(p->patternvoices) {
      if(p->patternvoices->nevents) {
	free(p->patternvoices->events);
      }

      free(p->patternvoices);
    }

  }

  if(cfg->patterns) {
    free(cfg->patterns);
  }
}

void qn_free_context(qn_context_t* ctx) {

  qn_config_t* cfg = ctx->config;

  int i;
  for(i=0; i<cfg->noutports; i++) {
    jack_port_unregister(ctx->run->client, ctx->run->ports[i]->jackport);
    free(ctx->run->ports[i]);
  }
  free(ctx->run->ports);
  free(ctx->run);

  qn_free_config(cfg);
  free(cfg);

  free(ctx);
}

int cmp_outevent(const void* a, const void* b) {
  const qn_outevent_t* ea = a;
  const qn_outevent_t* eb = b;

  // early message before late ones
  if(ea->at < eb->at) {
    return -1;
  } else if(ea->at > eb->at) {
    return 1;
  }

  // real-time (clock) messages over channel messages
  int as = ((ea->data[0] & 0xf8) == 0xf8);
  int bs = ((eb->data[0] & 0xf8) == 0xf8);
  if(as && !bs) {
    return -1;
  } else if(!as && bs) {
    return 1;
  }

  return 0;
}
