#include "../config.h"
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <jack/jack.h>
#include "context.h"
#include "lex.h"
#include "realtime.h"


static void usage(char* progname) {
  char* usage = "usage: %s <filename>\n\n"
    "Parse the input file and run the jack sequencer client.\n";
  fprintf(stderr,usage,progname);
}


int main(int argc, char** argv) {

  if(argc < 2) {
    usage(argv[0]);
    return 1;
  }

  qn_config_t* config = parse_config(argv[1]);


  /* Portable SIGINFO - if available, need to tell OS, otherwise use SIGUSR1 */
#if defined(SIGINFO)
  /* OS might not even pass SIGINFO to us, use sigaction to add w/ SIG_HOLD  */
  struct sigaction siginfoaction;
  siginfoaction.sa_handler = SIG_HOLD;
  sigemptyset(&siginfoaction.sa_mask);
  siginfoaction.sa_flags = 0;
  sigaction(SIGINFO, &siginfoaction, NULL);
#else
#  define SIGINFO SIGUSR1
#endif

  /* Jack is doing its own signal handling, which interferes with what we   */
  /* do here in the main thread. To work around this, set up signal mask    */
  /* of signals not to be handled before creating the jack client, as it'll */
  /* inherit it, so we can handle our signals as desired, in the main loop. */
  sigset_t ss;
  sigemptyset(&ss);
  sigaddset(&ss, SIGTERM);
  sigaddset(&ss, SIGINT);
  sigaddset(&ss, SIGINFO);
  sigprocmask(SIG_BLOCK, &ss, NULL);


  /* now open/create jack client */
  jack_client_t* client;
  jack_status_t status;
  if((client = jack_client_open (PACKAGE_NAME, JackNoStartServer, &status)) == 0) {
    fprintf (stderr, "Couldn't open jack client: %d\n", status);
    return 1;
  }

  /* qn_config_t* config = qn_get_fake_config(); // TODO: parse from a file */
  qn_context_t* ctx = qn_init_context(config,client);
  qn_reset_playhead(ctx);

  jack_set_process_callback (client, process, ctx);

  if(jack_set_timebase_callback(client,0,timebase,ctx)) {
    fprintf (stderr, "quincer requires to be timebase master, but was denied\n");
    return 1;
  }

  if (jack_activate(client)) {
    fprintf (stderr, "cannot activate client\n");
    return 1;
  }

  /* run until interrupted */
  while (1) {
    int s;
    if (sigwait(&ss, &s)) {
      fprintf(stderr, "fatal: signal handling error, abort.\n");
      break;
    }
    /* default shutdown signals, e.g. ^C */
    if (s == SIGTERM || s == SIGINT) {
      fprintf(stderr, "signal received, exiting ...\n");
      break;
    }
    /* stats requests signal handler (usually bound to ^T on BSDs) */
    if (s == SIGINFO) {
      fprintf(stderr, "sample rate: %d\n", jack_get_sample_rate(client));
      fprintf(stderr, "bar/beat/tick: %d/%d/%d\n", ctx->run->tbm_bar, ctx->run->tbm_beat, ctx->run->tbm_tick);
    }
  };

  qn_free_context(ctx);

  jack_client_close(client);
  return 0;
}
