set pagination off

dprintf accumulate_pattern_events:increment_nout,"  event %d at %d (%d - %d + (%d - %d)), pitch %d, playhead at %d, didnt break because %d + %d < %d\n",k,outev->at,evtime,pv->playhead_tmp[evtset],fill_last_nframes_orig,fill_last_nframes_in_pv,outev->data[1],pv->playhead[CURRENT],range_start,evtime,pv_len
cond 1 ((ev.data[0] & 0xf0) == 0x90) && (evtset==CURRENT)

dprintf accumulate_clock_events:clock_ticked,"  clock beat at %d\n",next_tick
cond 2 is_beat_start

# dprintf process:pre_accum_events,"process, awaiting %d\n",run->awaiting_common_start_beat
# cond 3 (i==0)

# dprintf accumulate_pattern_events:looping_event_candidates,"  event %d, if %d < %d < %d\n",k,range_start,evtime,range_end
# cond 3 (evtset==CURRENT) && (k==6)

# dprintf accumulate_pattern_events:maybe_break_event,"  skip event if %d + %d >= %d\n",range_start,evtime,pv_len
# cond 4 ((ev.start_tick % 4) == 0) && ((ev.data[0] & 0xf0) == 0x90)

# dprintf set_playheads:set_playheads_zero,"  set_playheads_zero\n"
# cond 4 (evtset == CURRENT) && (k == 0)

# dprintf set_playheads:set_playheads_value,"  set_playheads_value %d\n",pv->playhead_tmp[evtset]
# cond 5 (evtset == CURRENT) && (k == 0)

# dprintf accumulate_pattern_events:accum_one_pv,"  %d < range < %d, pv_end %d\n",pv->playhead_tmp[CURRENT],pv->playhead_tmp[CURRENT]+fill_last_nframes, pv_end
# cond 4 (evtset==CURRENT)

# dprintf accumulate_pattern_events:save_out_nframes,"  fill_last_nframes_in_pv %d\n",fill_last_nframes_in_pv
# cond 7 (evtset==CURRENT)

# dprintf accumulate_pattern_events:inner_loop_end,"  fill_last_nframes %d til_curr_pattern_end %d\n",fill_last_nframes,til_curr_pattern_end
# cond 7 (evtset==CURRENT)

# b accumulate_pattern_events:inner_loop_end if (evtset==CURRENT) && (fill_last_nframes > til_curr_pattern_end)

# dprintf accumulate_pattern_events:pv_no_wrap,"  no_wrap\n"
# cond 8 (evtset==CURRENT)

run work.qn